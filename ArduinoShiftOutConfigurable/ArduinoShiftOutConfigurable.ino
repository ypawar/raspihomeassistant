#include "config.h"
#include <SPI.h> // including the SPI library

//MSB first order
byte state_output[NUMBER_OF_SHIFT_OUT_CHIPS];

byte state=0;//Variable to hold the state of the pin requested currently

//Variable to hold API command and parameters
byte command;
byte pin;
byte value;
void setup() 
{
  //Setup SPI hardware
  SPI.setBitOrder(LSBFIRST);
  SPI.setDataMode(SPI_MODE0);
  SPI.setClockDivider(SPI_CLOCK_DIV2);
  SPI.begin();
  //Setup serial interface
  Serial.begin(9600);
  //Set pins as output
  pinMode(pload_out,OUTPUT);
  pinMode(10,OUTPUT);
  pinMode(out_enable,OUTPUT);
  digitalWrite(out_enable,HIGH);//Disable shift register output till we fill all with zeroes
  write_zeroes();//write zeroes to state array
  write_out();//flush out the array
  digitalWrite(out_enable,LOW);  
}

//One byte write function
void write_out()
{
    digitalWrite(pload_out,LOW);
    for (byte i=NUMBER_OF_SHIFT_OUT_CHIPS; i > 0 ; i--)
    {
      if (is_active_low[i-1])
      {
        SPI.transfer(~state_output[i-1]);
      }
      else
      {
        SPI.transfer(state_output[i-1]);
      }
    }
    digitalWrite(pload_out,HIGH);
}

boolean write_state_out(byte pin_number, byte pin_state)
{
  //Return false if pin number exceed configured pins
  if (pin_number >= NUMBER_OF_SHIFT_OUT_CHIPS *8)
  {
    return false;
  }
  else
  {
    bitWrite(state_output[NUMBER_OF_SHIFT_OUT_CHIPS - 1 - (pin_number / 8)], pin % 8, pin_state);
    return true;
  }
}

boolean read_state_out(byte pin_number)
{
  //Return false if pin number exceed configured pins
  if (pin_number >= NUMBER_OF_SHIFT_OUT_CHIPS *8)
  {
    return false;
  }
  else
  {
    state = bitRead(state_output[NUMBER_OF_SHIFT_OUT_CHIPS - 1 - (pin_number / 8)], pin_number % 8);
    return true;
  }
}

void write_zeroes()
{
  for (byte i = 0; i < NUMBER_OF_SHIFT_OUT_CHIPS * 8 ; i++)
  {
    write_state_out(i,0);
  }
}

void loop() 
{  
  if (Serial.available()>0)
  {
    command = Serial.parseInt();
    if (command == 1)//get state of the pin
    {
      pin = Serial.parseInt();
      if (read_state_out(pin))
      {
        Serial.println(state);
      }
      else
      {
        Serial.println("ERROR");
      }
    }
    if (command == 2)//set pin state
    {
      pin = Serial.parseInt();
      value = Serial.parseInt();
      if (write_state_out(pin, value))
      {
        write_out();//flush out the array
        Serial.println("OK");
      }
      else
      {
        Serial.println("ERROR");
      }
    }
  }

}
