#define NUMBER_OF_SHIFT_OUT_CHIPS   3
//define whether relay board is active low or not from left to right
const boolean is_active_low[] = { false, true, false };
#define pload_out 7 //Connects to Parallel load pin 12 of the 74595
#define out_enable 4 //Connects to Output Enable pin 13 of the 74595
#define pload_in 14 //Connects to Parallel load pin 1 of the 74165
#define clk_enable_in 15 //Connects to Clock Enable pin 15 of the 74165
