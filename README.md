Install Hassbian On Raspberry Pi
URL: https://home-assistant.io/docs/installation/hassbian/installation/

The default username is pi and the password is raspberry.
Check status of installation script if it takes longer time to install.
sudo systemctl status install_homeassistant.service
Default location:  http://hassbian.local:8123

Setting up Access Point in a standalone network:

URL: https://www.raspberrypi.org/documentation/configuration/wireless/access-point.md

sudo apt-get update
sudo apt-get dist-upgrade
sudo apt-get install -y dnsmasq hostapd

Since the configuration files are not ready yet, turn the new software off as follows: 
sudo systemctl stop dnsmasq
sudo systemctl stop hostapd

Configuring a static IP
sudo nano /etc/dhcpcd.conf
interface wlan0

static ip_address=192.168.1.1/24
static routers=192.168.1.1
static domain_name_servers=192.168.1.1

Configuring the DHCP server (dnsmasq)
sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig
sudo nano /etc/dnsmasq.conf
interface=wlan0
dhcp-range=192.168.1.2,192.168.1.20,255.255.255.0,24h


sudo nano /etc/hostapd/hostapd.conf

interface=wlan0
driver=nl80211
ssid=ssid_name
hw_mode=g
channel=7
wmm_enabled=0
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=password
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP


sudo nano /etc/default/hostapd

DAEMON_CONF="/etc/hostapd/hostapd.conf"

sudo systemctl restart networking
sudo systemctl restart dnsmasq
sudo systemctl restart hostapd



sudo apt-get install apache2
sudo a2nemod rewrite
sudo a2nemod proxy
sudo a2nemod proxy_http
sudo a2nemod proxy_balancer
sudo a2nemod proxy_wstunnel

sudo systemctl restart apache2



sudo nano /etc/apache2/sites-enabled/hass.conf

<VirtualHost *:80>
  ProxyPreserveHost On
  ProxyRequests off
  ProxyPass / http://localhost:8123/
  ProxyPassReverse / http://localhost:8123/
  ProxyPass /api/websocket ws://localhost:8123/api/websocket
  ProxyPassReverse /api/websocket ws://localhost:8123/api/websocket

  RewriteEngine on
  RewriteCond %{HTTP:Upgrade} =websocket [NC]
  RewriteRule /(.*)  ws://localhost:8123/$1 [P,L]
  RewriteCond %{HTTP:Upgrade} !=websocket [NC]
  RewriteRule /(.*)  http://localhost:8123/$1 [P,L]
</VirtualHost>

sudo rm sites-enabled/000-default.conf


sudo systemctl reload apache2

Install apache WSGI mod for python 2.7 to host the Arduino Hardware API server.
sudo apt-get install -y libapache2-mod-wsgi

Install pip for python package management
sudo apt-get install -y python-pip

sudo pip install virtualenv

Create a virtual environment for the API application
sudo virtualenv /srv/webapp

Change permissions
sudo chown pi:pi -R /srv/webapp/

Activate this new virtualenv
source /srv/webapp/bin/activate

Install required packages
pip install flask_restful RPi.GPIO

Put raspiapi.py file in /srv/webapp/raspihass

touch /srv/webapp/raspihass.wsgi

#!/srv/webapp/bin/python
import sys
sys.path.insert(0,"/srv/webapp/")
from raspihass.raspiapi import app as application
application.secret_key = 'secretpassword'

sudo nano /etc/apache2/sites-enabled/restapi.conf

<VirtualHost *:8080>
		WSGIDaemonProcess restapi python-home=/srv/webapp
		WSGIApplicationGroup %{GLOBAL}
		WSGIProcessGroup restapi
		WSGIScriptAlias / /srv/webapp/raspihass.wsgi
		<Directory /srv/webapp/raspihass/>
			Require all granted
		</Directory>
		ErrorLog ${APACHE_LOG_DIR}/error.log
		LogLevel warn
		CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

sudo nano /etc/apache2/ports.conf
Listen localhost:8080

Set permissions
sudo usermod www-data -aG gpio


sudo nano  /etc/systemd/system/home-assistant@homeassistant.service

[Unit]
Description=Home Assistant for %i
After=network.target apache2.service

[Service]
Type=simple
User=%i
ExecStartPre=/usr/bin/curl http://localhost:8080/switch/0
ExecStart=/srv/homeassistant/bin/hass
SendSIGKILL=no

[Install]
WantedBy=multi-user.target


Copy/Replace provided files in /home/homeassistant/.homeassistant

Install and setup deluge torrent client
https://discourse.osmc.tv/t/how-to-always-on-remotely-accessible-torrent-server/22006

sudo apt-get install deluged deluge-web
sudo update-rc.d deluged remove
sudo rm /etc/init.d/deluged
sudo nano /etc/systemd/system/deluged.service
[Unit]
Description=Deluge Bittorent Daemon
After=network-online.target

[Service]
Type=simple
User=debian-deluged
Group=debian-deluged
UMask=007

ExecStart=/usr/bin/deluged -d

Restart=on-failure

# Configures the time to wait before service is stopped forcefully.
TimeoutStopSec=300

[Install]
WantedBy=multi-user.target

sudo nano /etc/systemd/system/deluge-web.service
[Unit]
Description=Deluge Bittorent Web Interface
After=network-online.target

[Service]
Type=simple
User=debian-deluged
Group=debian-deluged
UMask=027

ExecStart=/usr/bin/deluge-web

Restart=on-failure

[Install]
WantedBy=multi-user.target


sudo systemctl daemon-reload
sudo systemctl enable deluged.service
sudo systemctl enable deluge-web.service
sudo systemctl start deluged
sudo systemctl start deluge-web.service