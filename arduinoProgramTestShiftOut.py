chips = 3

import serial
import time

ser = serial.Serial("COM4", 9600)
cmd = ''
delay = 3


def writetoserial():
    for i in range(8 * chips):
    # for i in range(8 * chips - 1, -1, -1):
        # Read all pin values, should be zero
        cmd = '1/{}/'.format(i)
        ser.write(cmd)
        state = ser.readline().strip()
        if state != '0':
            print "Not initialised to zero"
        else:
            print "Initial state OK"
        # Activate pins one by one and read again to confirm the state
        cmd = '2/{}/1/'.format(i)
        ser.write(cmd)
        if ser.readline().strip() != 'OK':
            print "Response invalid"
        else:
            print "Write Response OK"
        cmd = '1/{}/'.format(i)
        ser.write(cmd)
        state = ser.readline().strip()
        if state != '1':
            print "Pin state not changed"
        else:
            print "Pin state change OK"
        time.sleep(delay)
    # Check error conditions
    # Read greater than configured pins
    cmd = '1/{}/'.format(8 * chips + 1)
    ser.write(cmd)
    state = ser.readline().strip()
    if state == "ERROR":
        print "Read error OK"
    else:
        print "Read error response invalid"

    cmd = '2/{}/0/'.format(8 * chips + 1)
    ser.write(cmd)
    state = ser.readline().strip()
    if state == "ERROR":
        print "Write error OK"
    else:
        print "Write error response invalid"


writetoserial()
