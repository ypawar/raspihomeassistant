from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from flask import jsonify
import json


import RPi.GPIO as gpio
import time


gpio.setmode(gpio.BOARD)
gpio.setwarnings(False)

o_latch=36
o_clock=38
o_data=40

pause=0.001
#state = 0
pinstate=[]
gpio.setup(o_latch,gpio.OUT)
gpio.output(o_latch,0)

gpio.setup(o_clock,gpio.OUT)
gpio.output(o_clock,0)

gpio.setup(o_data,gpio.OUT)
gpio.output(o_data,0)

def tick():
    gpio.output(o_clock,1)
    time.sleep(pause)
    gpio.output(o_clock,0)
    time.sleep(pause)

def latch():
    gpio.output(o_latch,1)
    time.sleep(pause)
    gpio.output(o_latch,0)
    time.sleep(pause)

def shiftOut(value,cnt=1):
    #print(value)
    for i in range(8*cnt):
        #gpio.output(o_data,(value>>i)&1)
        gpio.output(o_data,not value[i]['state'])
        time.sleep(pause)
        tick()
    latch()

def initPinState():
    global pinstate
    for i in range(8):
        pinstate.append({'state':False})
    shiftOut(pinstate)
    #print(pinstate)

def setPinState():
    global state
    global pinstate
    for i in range(8):
        #if ((state>>i)&1):
            pinstate[i]={'state':True}
        #else:
            pinstate[i]={'state':False}

app = Flask(__name__)
api = Api(app)


# state = 0

class switch(Resource):

    def get(self,sw_id):
        # print sw_id
        # global state
        global pinstate
        if pinstate[sw_id]['state'] == True:
            return {"is_active": "true"}
        return {"is_active": "false"}

    def post(self,sw_id):
        # global state
        # print request.data
        # print json.loads(request.data)
        s = json.loads(request.data.decode())
        # print type(s)
        #print (s)
        if s['active'] == 'true':
            global pinstate
            pinstate[sw_id]['state']=True
            shiftOut(pinstate)
            # state = True
            # print state
            return #{"is_active": "true"}
        else:
            #global pinstate
            pinstate[sw_id]['state']=False
            shiftOut(pinstate)
            # state = False
            # print state
            return #{"is_active": "false"}

api.add_resource(switch,'/switch/<int:sw_id>')
initPinState()
if __name__ == '__main__':

    app.run(host='0.0.0.0',debug=True,port=8000)

